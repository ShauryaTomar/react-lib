import { useContext } from 'react';
import { User } from './interfaces';
import TruIAMContext, { TruIAMContextInterface } from './truiam-context';

const truIAMLib = <TUser extends User = User>(
  context = TruIAMContext
): TruIAMContextInterface<TUser> =>
  useContext(context) as TruIAMContextInterface<TUser>;

export default truIAMLib;
