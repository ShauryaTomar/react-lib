import React from "react";
import {User, initialAuthState, AuthState} from './interfaces'
export interface TruIAMContextInterface<TUser extends User = User> extends AuthState<TUser>{
  domain: string,
  clientId?: string,
  clientSecret?: string,
  redirectUri?: string,
  handleLogin: ()=>void;
  handleLogout: ()=>void;
}
const stub = (): never => {
  throw new Error('You forgot to wrap your component in <TruIAMProvider>.');
};

const defaultState = {
  ...initialAuthState,
  domain: "",
  clientId: "",
  clientSecret: "",
  redirectUri: "",
  handleLogin: stub,
  handleLogout: stub
};

const TruIAMContext = React.createContext<TruIAMContextInterface>(defaultState);
export default TruIAMContext;
