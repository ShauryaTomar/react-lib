import React, { useRef, useMemo, useReducer, useEffect, useState, useCallback } from "react";
import TruIAMClient from "./truiam-client";
import {
  TruIAMProviderInterface,
  initialAuthState,
  User,
  LogoutUrlOptions,
} from "./interfaces";
import TruIAMContext from "./truiam-context";
import { reducer } from "./truiam-reducer";

//checkUp Functions
const CODE_REGEX = /[?&]code=[^&]+/;
const TOKEN = /[?&]accessToken=[^&]+/;
const ERROR_REGEX = /[?&]error=[^&]+/;
export const hasAuthParams = (searchParams = window.location.search): boolean =>
  CODE_REGEX.test(searchParams) || ERROR_REGEX.test(searchParams) || TOKEN.test(searchParams);

const TruIAMProvider = (args: TruIAMProviderInterface): JSX.Element => {
  const { children, context = TruIAMContext, ...clientArgs } = args;
  const [state, dispatch] = useReducer(reducer, initialAuthState);
  const didInitialise = useRef(false);

  const [client] = useState(() => new TruIAMClient(clientArgs));
  useEffect(() => {
    if (didInitialise.current) {
      return;
    }
    didInitialise.current = true;
    (async (): Promise<void> => {
      try {
        let user: User | undefined;
        if (hasAuthParams()) {
          const result = await client.handleParams();
    
          if (result.status) {
            window.history.replaceState({}, "", `/`);
            user = await client.getUser();
            
          }
          else{
            window.history.replaceState({},'',`/`);
            throw result.message;
          }
        
        } else {
          
          if (localStorage.getItem("isAuthenticated")==="true") {
            await client.reloadTokenFromStorage();
            user = await client.getUser();
          }
        
        }
       
        dispatch({ type: "INITIALISED", user });
      } catch (error) {
        console.log(error);
        dispatch({ type: "ERROR", error: error });
      }
    })();
  }, [client]);
  const handleLogin = () => {
    window.location.href =
      "https://" +
      clientArgs.domain +
      "/universalAuth/login-signup?redirectUri=" +
      clientArgs.redirectUri+"&clientid="+clientArgs.clientId;
  };

  const handleLogout = useCallback(
    (opts: LogoutUrlOptions = {}) => {
    
      localStorage.removeItem("isAuthenticated");
      localStorage.removeItem("accessToken");
      localStorage.removeItem("idToken");
      localStorage.removeItem("token");
      dispatch({ type: "LOGOUT" });
      if(opts.returnTo)
        window.location.assign(opts.returnTo);
    },[client]);

  const contextValue = useMemo(() => {
    return {
      ...state,
      ...clientArgs,
      handleLogin,
      handleLogout,
    };
  }, [state, handleLogin, clientArgs, handleLogout]);
  return <context.Provider value={contextValue}>{children}</context.Provider>;
};

export default TruIAMProvider;
