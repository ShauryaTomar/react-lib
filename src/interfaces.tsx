import { TruIAMContextInterface } from "./truiam-context";

export const initialAuthState: AuthState = {
  isAuthenticated: false,
  isLoading: true,
};
interface Error {
  name: string;
  message: string;
  stack?: string;
}

export interface LogoutUrlOptions {
    returnTo?: string | URL;
}
export type CacheLocation = "memory" | "localstorage";
export type AppState = {
  returnTo?: string;
  [key: string]: any;
};
 export interface Params {
  url: string;
  headers: any;
  method?: string;
  body: any;
}
export interface TruIAMClientInterface {
  domain: string;
  clientId: string;
  redirectUri: string;
  tokenResult?: string;
};
export interface TruIAMProviderInterface extends TruIAMClientInterface {
  children?: React.ReactNode;
  onRedirectCallback?: (user?: User) => void;

  context?: React.Context<TruIAMContextInterface>;
};

export interface AuthState<TUser extends User = User> {
  error?: Error;
  isAuthenticated: boolean;
  isLoading: boolean;
  user?: TUser;
}

export declare class User {
  name?: string;
  given_name?: string;
  family_name?: string;
  middle_name?: string;
  firstName?: string;
  lastName?: string;
  nickname?: string;
  preferred_username?: string;
  providerName?: string;
  profile?: string;
  picture?: string;
  website?: string;
  email?: string;
  email_verified?: boolean;
  gender?: string;
  birthdate?: string;
  zoneinfo?: string;
  locale?: string;
  phone_number?: string;
  phone_number_verified?: boolean;
  address?: string;
  updated_at?: string;
  sub?: string;
  [key: string]: any;
}

export interface IdToken {
  at_hash: string,
  email_verified: boolean,
  sub :string,
  email:string,
  iss:string,
  aud:string,
  token_use:string,
  auth_time:string,
  exp:string,
  iat:string,
  identities: any,
  jti:string
  [key: string]: any;
}

export interface AuthenticationResult {
  code?: string;
  error?: string;
  error_description?: string;
}

export interface TokenEndpointOptions {
  baseUrl: string;
  client_id: string;
  grant_type: string;
  timeout?: number;
  auth0Client: any;
  useFormData?: boolean;
  [key: string]: any;
}
/**
* @ignore
*/
export declare type TokenEndpointResponse = {
  id_token: string;
  access_token: string;
  refresh_token?: string;
  expires_in: number;
  scope?: string;
};

export interface OAuthTokenOptions extends TokenEndpointOptions {
  code_verifier: string;
  code: string;
  redirect_uri: string;
  audience: string;
  scope: string;
}
/**
* @ignore
*/
export interface RefreshTokenOptions extends TokenEndpointOptions {
  refresh_token: string;
}
/**
* @ignore
*/
export interface JWTVerifyOptions {
  iss: string;
  aud: string;
  id_token: string;
  nonce?: string;
  leeway?: number;
  max_age?: number;
  organizationId?: string;
  now?: number;
}

export declare type FetchOptions = {
  method?: string;
  headers?: Record<string, string>;
  credentials?: 'include' | 'omit';
  body?: string;
  signal?: AbortSignal;
};

