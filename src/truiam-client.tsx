import { TruIAMClientInterface, Params, User} from "./interfaces";
import { decodedToken } from "./utils";

export default class TruIAMClient {

  private tokenResult : any;
  public status : boolean;
  // private async _decodeToken(
  //   token: string,
  // ) {;

  //   return decodeToken({
  //     iss: this.tokenIssuer,
  //     aud: this.options.clientId,
  //     id_token,
  //     nonce,
  //     organizationId,
  //     leeway: this.options.leeway,
  //     max_age: parseNumber(this.options.authorizationParams.max_age),
  //     now
  //   });
  // }
  constructor(private args: TruIAMClientInterface) {
    this.tokenResult = "";
    this.status = false;
  }
  public async getUser<TUser extends User>(): Promise<TUser | undefined> {

    var user = decodedToken(this.tokenResult.id_token);
  
    return user as TUser;
  }
  public async reloadTokenFromStorage(){
    const token = localStorage.getItem("token");
    if(token)
      this.tokenResult = JSON.parse(token);
  }


  public async getToken(options: Params) {

    const response = await fetch(options.url, options);
    const data = await response.json();
    return [data.status, data.message, data?.data];
   
  }

  public async handleParams(
    url: string = window.location.search
  ): Promise<any> {
    const queryParams = new URLSearchParams(url.toString());

    const code = queryParams.get("code");
    const error = queryParams.get("error");
    const error_description = queryParams.get("error_description");
    var accessToken = queryParams.get("accessToken");

    if (error && error_description) {
      return {status:false, message:{error:error, description: error_description}};
      // to be handled ------------------------------------------------------
    } 
    else if (code) {
      var body = JSON.stringify({
        clientId: this.args.clientId,
        callbackurl: this.args.redirectUri,
        code: code,
      });
      const domain = this.args.domain.split(".")[0];
      const options: Params = {
        url: "https://api.truiamassaas.link/dev/api/auth/token",
        headers: {
          "Content-Type": "application/json",
          idp: "cognito",
          domain: domain,
        },
        method: "POST",
        body: body,
      };
      const [status,message, tokenResult]  = await this.getToken(options);
      this.status = status;
     
      
      if(status === true && message === "Success"){
        localStorage.setItem("isAuthenticated", "true");
        this.tokenResult = tokenResult;
    
        localStorage.setItem("token",JSON.stringify(this.tokenResult));
        localStorage.setItem("idToken",this.tokenResult['id_token']);
        localStorage.setItem("accessToken",this.tokenResult['access_token']);
        
        
      }
      else{
        localStorage.setItem("isAuthenticated", "false");
      }
      return {status:status, message:'Authenticated'};
      
    }
    else if(accessToken){
        
        var refreshToken = queryParams.get("refreshToken");
        var idToken = queryParams.get("idToken");
        this.tokenResult = {
          'id_token' : idToken,
          'refresh_token': refreshToken,
          'access_token': accessToken,
        };
        localStorage.setItem("isAuthenticated", "true");
        localStorage.setItem("token",JSON.stringify(this.tokenResult));
        localStorage.setItem("idToken",this.tokenResult['id_token']);
        localStorage.setItem("accessToken",this.tokenResult['access_token']); 
        return {status:true, message:'Loaded Authenticated User'};
    }

  }

}
