import jwt_decode from "jwt-decode";
import { IdToken } from "./interfaces";

export const decodedToken = (opts:string) => {
    const idToken:IdToken = jwt_decode(opts);
  
    let user = {
      'email': idToken["email"],
      'firstName':idToken["custom:firstName"],
      'lastName':idToken["custom:lastName"],
      'email_verified': idToken["email_verified"],
      'name': idToken["cognito:username"]
      
    }

    return user;
  }