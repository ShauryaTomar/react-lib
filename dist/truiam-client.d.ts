import { TruIAMClientInterface, Params, User } from "./interfaces";
export default class TruIAMClient {
    private args;
    private tokenResult;
    status: boolean;
    constructor(args: TruIAMClientInterface);
    getUser<TUser extends User>(): Promise<TUser | undefined>;
    reloadTokenFromStorage(): Promise<void>;
    getToken(options: Params): Promise<any[]>;
    handleParams(url?: string): Promise<any>;
}
//# sourceMappingURL=truiam-client.d.ts.map