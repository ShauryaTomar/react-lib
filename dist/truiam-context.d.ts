import React from "react";
import { User, AuthState } from './interfaces';
export interface TruIAMContextInterface<TUser extends User = User> extends AuthState<TUser> {
    domain: string;
    clientId?: string;
    clientSecret?: string;
    redirectUri?: string;
    handleLogin: () => void;
    handleLogout: () => void;
}
declare const TruIAMContext: React.Context<TruIAMContextInterface<User>>;
export default TruIAMContext;
//# sourceMappingURL=truiam-context.d.ts.map