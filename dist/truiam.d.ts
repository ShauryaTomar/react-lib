/// <reference types="react" />
import { User } from './interfaces';
import { TruIAMContextInterface } from './truiam-context';
declare const truIAMLib: <TUser extends User = User>(context?: import("react").Context<TruIAMContextInterface<User>>) => TruIAMContextInterface<TUser>;
export default truIAMLib;
//# sourceMappingURL=truiam.d.ts.map