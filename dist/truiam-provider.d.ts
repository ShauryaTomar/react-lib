/// <reference types="react" />
import { TruIAMProviderInterface } from "./interfaces";
export declare const hasAuthParams: (searchParams?: string) => boolean;
declare const TruIAMProvider: (args: TruIAMProviderInterface) => JSX.Element;
export default TruIAMProvider;
//# sourceMappingURL=truiam-provider.d.ts.map