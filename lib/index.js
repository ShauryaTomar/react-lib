"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.truIAMLib = exports.TruIAMProvider = void 0;
var truiam_provider_1 = require("./truiam-provider");
Object.defineProperty(exports, "TruIAMProvider", { enumerable: true, get: function () { return __importDefault(truiam_provider_1).default; } });
var truiam_1 = require("./truiam");
Object.defineProperty(exports, "truIAMLib", { enumerable: true, get: function () { return __importDefault(truiam_1).default; } });
//# sourceMappingURL=index.js.map