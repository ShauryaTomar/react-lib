"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.decodedToken = void 0;
var jwt_decode_1 = __importDefault(require("jwt-decode"));
var decodedToken = function (opts) {
    var idToken = (0, jwt_decode_1.default)(opts);
    var user = {
        'email': idToken["email"],
        'firstName': idToken["custom:firstName"],
        'lastName': idToken["custom:lastName"],
        'email_verified': idToken["email_verified"],
        'name': idToken["cognito:username"]
    };
    return user;
};
exports.decodedToken = decodedToken;
//# sourceMappingURL=utils.js.map