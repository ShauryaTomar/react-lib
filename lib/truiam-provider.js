"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.hasAuthParams = void 0;
var react_1 = __importStar(require("react"));
var truiam_client_1 = __importDefault(require("./truiam-client"));
var interfaces_1 = require("./interfaces");
var truiam_context_1 = __importDefault(require("./truiam-context"));
var truiam_reducer_1 = require("./truiam-reducer");
//checkUp Functions
var CODE_REGEX = /[?&]code=[^&]+/;
var TOKEN = /[?&]accessToken=[^&]+/;
var ERROR_REGEX = /[?&]error=[^&]+/;
var hasAuthParams = function (searchParams) {
    if (searchParams === void 0) { searchParams = window.location.search; }
    return CODE_REGEX.test(searchParams) || ERROR_REGEX.test(searchParams) || TOKEN.test(searchParams);
};
exports.hasAuthParams = hasAuthParams;
var TruIAMProvider = function (args) {
    var children = args.children, _a = args.context, context = _a === void 0 ? truiam_context_1.default : _a, clientArgs = __rest(args, ["children", "context"]);
    var _b = (0, react_1.useReducer)(truiam_reducer_1.reducer, interfaces_1.initialAuthState), state = _b[0], dispatch = _b[1];
    var didInitialise = (0, react_1.useRef)(false);
    var client = (0, react_1.useState)(function () { return new truiam_client_1.default(clientArgs); })[0];
    (0, react_1.useEffect)(function () {
        if (didInitialise.current) {
            return;
        }
        didInitialise.current = true;
        (function () { return __awaiter(void 0, void 0, void 0, function () {
            var user, result, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 9, , 10]);
                        user = void 0;
                        if (!(0, exports.hasAuthParams)()) return [3 /*break*/, 5];
                        return [4 /*yield*/, client.handleParams()];
                    case 1:
                        result = _a.sent();
                        if (!result.status) return [3 /*break*/, 3];
                        window.history.replaceState({}, "", "/");
                        return [4 /*yield*/, client.getUser()];
                    case 2:
                        user = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        window.history.replaceState({}, '', "/");
                        throw result.message;
                    case 4: return [3 /*break*/, 8];
                    case 5:
                        if (!(localStorage.getItem("isAuthenticated") === "true")) return [3 /*break*/, 8];
                        return [4 /*yield*/, client.reloadTokenFromStorage()];
                    case 6:
                        _a.sent();
                        return [4 /*yield*/, client.getUser()];
                    case 7:
                        user = _a.sent();
                        _a.label = 8;
                    case 8:
                        dispatch({ type: "INITIALISED", user: user });
                        return [3 /*break*/, 10];
                    case 9:
                        error_1 = _a.sent();
                        console.log(error_1);
                        dispatch({ type: "ERROR", error: error_1 });
                        return [3 /*break*/, 10];
                    case 10: return [2 /*return*/];
                }
            });
        }); })();
    }, [client]);
    var handleLogin = function () {
        window.location.href =
            "https://" +
                clientArgs.domain +
                "/universalAuth/login-signup?redirectUri=" +
                clientArgs.redirectUri + "&clientid=" + clientArgs.clientId;
    };
    var handleLogout = (0, react_1.useCallback)(function (opts) {
        if (opts === void 0) { opts = {}; }
        localStorage.removeItem("isAuthenticated");
        localStorage.removeItem("accessToken");
        localStorage.removeItem("idToken");
        localStorage.removeItem("token");
        dispatch({ type: "LOGOUT" });
        if (opts.returnTo)
            window.location.assign(opts.returnTo);
    }, [client]);
    var contextValue = (0, react_1.useMemo)(function () {
        return __assign(__assign(__assign({}, state), clientArgs), { handleLogin: handleLogin, handleLogout: handleLogout });
    }, [state, handleLogin, clientArgs, handleLogout]);
    return react_1.default.createElement(context.Provider, { value: contextValue }, children);
};
exports.default = TruIAMProvider;
//# sourceMappingURL=truiam-provider.js.map