"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var interfaces_1 = require("./interfaces");
var stub = function () {
    throw new Error('You forgot to wrap your component in <TruIAMProvider>.');
};
var defaultState = __assign(__assign({}, interfaces_1.initialAuthState), { domain: "", clientId: "", clientSecret: "", redirectUri: "", handleLogin: stub, handleLogout: stub });
var TruIAMContext = react_1.default.createContext(defaultState);
exports.default = TruIAMContext;
//# sourceMappingURL=truiam-context.js.map