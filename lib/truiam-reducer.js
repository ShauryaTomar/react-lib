"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.reducer = void 0;
/**
 * Handles how that state changes in the `useAuth0` hook.
 */
var reducer = function (state, action) {
    var _a, _b;
    switch (action.type) {
        case 'LOGIN_POPUP_STARTED':
            return __assign(__assign({}, state), { isLoading: true });
        case 'LOGIN_POPUP_COMPLETE':
        case 'INITIALISED':
            return __assign(__assign({}, state), { isLoading: false, isAuthenticated: !!action.user, user: action.user, error: undefined });
        case 'HANDLE_REDIRECT_COMPLETE':
        case 'GET_ACCESS_TOKEN_COMPLETE':
            if (((_a = state.user) === null || _a === void 0 ? void 0 : _a.updated_at) === ((_b = action.user) === null || _b === void 0 ? void 0 : _b.updated_at)) {
                return state;
            }
            return __assign(__assign({}, state), { isAuthenticated: !!action.user, user: action.user });
        case 'LOGOUT':
            return __assign(__assign({}, state), { isAuthenticated: false, user: undefined });
        case 'ERROR':
            return __assign(__assign({}, state), { isLoading: false, error: action.error });
    }
};
exports.reducer = reducer;
//# sourceMappingURL=truiam-reducer.js.map