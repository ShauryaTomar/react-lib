"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = require("react");
var truiam_context_1 = __importDefault(require("./truiam-context"));
var truIAMLib = function (context) {
    if (context === void 0) { context = truiam_context_1.default; }
    return (0, react_1.useContext)(context);
};
exports.default = truIAMLib;
//# sourceMappingURL=truiam.js.map